package fpinscala.chapter3

import org.scalatest.{FlatSpec, Matchers}

class ListsSpec extends FlatSpec with Matchers {
  import Lists._
  
  "tail" should "removes the first element from list" in {
    val list = List(1, 2, 3)
    
    tail(list) shouldEqual list.tail
  }

  "setHead" should "replace the head element from a given list" in {
    val list = List(9, 2, 3)

    setHead(list, 1) shouldEqual List(1, 2, 3)
  }

  "drop" should "remove the first n elements from a list" in {
    val list = List(1, 2, 3, 4, 5)

    drop(list, 3) shouldEqual List(4, 5)
  }

  "dropWhile" should "remove while predicate is true" in {
    val predicate = (x: Int) => x <= 3
    val list = List(1, 2, 3, 4, 5, 6)

    dropWhile(list, predicate) shouldEqual List(4, 5, 6)
  }

  it should "do nothing if empty list" in {
    dropWhile(Nil, (x: Int) => x > 3) shouldEqual Nil
  }

  "init" should "remove last element from a list" in {
    val list = List(1, 2, 3, 4)

    init(list) shouldEqual List(1, 2, 3)
  }

  it should "return nil if nil" in {
    init(Nil) shouldEqual Nil
  }

  "home-made length using foldright" should "calculates list length" in {
    val list = List(1, 2, 3, 4)

    Lists.length(list) shouldEqual 4
  }

  "home-made foldLeft" should "be tail recursive" in {
    val list = List(1, 2, 3)
  
    Lists.foldLeft(list, 0)((a, b) => a + b) shouldEqual 6
  }

  "sumLeft" should "calculate the some of List's elements" in {
    val list = List(1, 2, 3, 4, 5)
  
    sumLeft(list) shouldEqual 15
  }

  "productLeft" should "compute the product of lists' values" in {
    val list = List(1, 2, 3, 4)
  
    productLeft(list) shouldEqual 24
  }

  "home-made length using foldLeft" should "calculates list length" in {
    val list = List(1, 2, 3, 4, 10)

    lengthLeft(list) shouldEqual 5
  }

  "reserve" should "reverse a list" in {
    val list = List(1, 2, 3)

    reverseLeft(list) shouldEqual List(3, 2, 1)
  }

  "foldRightLeft" should "behaves like foldRight function" in {
    val list = List(1, 2, 3, 4)

    foldRightLeft(list, Seq.empty[Int])((e, xs) => e +: xs) shouldEqual list
  }

  "appendLeft" should "add element to the end of list" in {
    val list = List(1, 2)

    appendLeft(list, List(10)) shouldEqual List(1, 2, 10)
  }

  "concat" should "concat a list of lists" in {
    val lists = List(List(1, 2, 3), List(4, 5))

    concat(lists) shouldEqual List(1, 2, 3, 4, 5)
  }

  "add1" should "increment by 1 every element on the list" in {
    val list = List(1, 2, 3, 4)

    add1(list) shouldEqual List(2, 3, 4, 5)
  }

  "double2string" should "convert double into string equivalent" in {
    val list = List(1.0, 1.2, 2.0, 2.89)

    double2string(list) shouldEqual List("1.0", "1.2", "2.0", "2.89")
  }

  "mapAsFold" should "apply a function to every element on a list" in {
    val list = List(1, 2, 3, 4)
    val f = (a: Int) => a * 100

    mapAsFold(list)(f) shouldEqual List(100, 200, 300, 400)
  }

  "filterAsFold" should "remove elements from a list given a predicate" in {
    val list = List(1, 2, 3, 4, 5)
    val f = (a: Int) => a % 2 == 0

    filterAsFold(list)(f) shouldEqual List(2, 4)
  }

  "flatMap" should "applies a function to all elements on the list" in {
    val list = List(1, 2, 3)
    val f = (a: Int) => List(a, a)

    flatMap(list)(f) shouldEqual List(1, 1, 2, 2, 3, 3)
  }

  "filterAsFlatMap" should "remove elements from a list given a predicate" in {
    val list = List(1, 2, 3, 4, 5)
    val f = (a: Int) => a % 2 == 0

    filterAsFlatMap(list)(f) shouldEqual List(2, 4)
  }
}
