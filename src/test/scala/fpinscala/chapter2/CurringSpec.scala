package fpinscala.chapter2

import org.scalatest.{Matchers, FlatSpec}

class CurringSpec extends FlatSpec with Matchers {
  import Curring._

  it should "currying a given function" in {
    val f = (a: Int, b: Int) => (a, b)

    curry(f)(10)(20) shouldEqual (10, 20)
  }

  "uncurry" should "decurrying a function" in {
    val f = (a: Int) => (b: Int) => b + a

    uncurry(f)(10, 20) shouldEqual 30
  }
}
