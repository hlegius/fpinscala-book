package fpinscala.chapter2

import org.scalatest.{FlatSpec, Matchers}

class ArraySortedSpec extends FlatSpec with Matchers {
  import ArraySorted._

  "isSorted" should "returns false if array is not sorted as function requires" in {
    isSorted(Array(1, 2, 3), (a: Int, b: Int) => b > a) shouldEqual false
  }

  it should "returns true if array is sorted" in {
    isSorted(Array(1, 2, 3), (a: Int, b: Int) => b < a) shouldEqual true
  }
}
