package fpinscala.chapter2

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks._

class FibonacciSpec extends FlatSpec with Matchers {
  import Fibonacci._

  it should "return 0 if 0 is given" in {
    fibo(0) shouldEqual 0
  }

  it should "return 0 if less than zero is given" in {
    fibo(-10) shouldEqual 0
  }

  it should "return 1 if one is given" in {
    fibo(1) shouldEqual 1
  }

  it should "return 1 if two is given" in {
    fibo(2) shouldEqual 1
  }

  it should "return a Table combination of values" in {
    val combinations = Table(
      ("input", "output"),
      (3, 2),
      (4, 3),
      (5, 5),
      (6, 8),
      (7, 13),
      (8, 21),
      (20, 6765)
    )

    forAll(combinations) { (input: Int, output: Int) =>
      fibo(input) shouldEqual output
    }
  }
}
