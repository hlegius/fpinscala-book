package fpinscala
package chapter2

import scala.annotation.tailrec

object Fibonacci {
  def fibo(x: Int): Int = {
    @tailrec
    def fibn(y: Int, cr: Int, pr: Int): Int = y match {
      case i if i <= 0 => pr
      case _ => fibn(y - 1, pr, cr + pr)
    }
  
    fibn(x, 1, 0)
  }
}
