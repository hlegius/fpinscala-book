package fpinscala
package chapter2

import scala.annotation.tailrec

object ArraySorted {
  def isSorted[A](as: Array[A], f: (A,A) => Boolean): Boolean = {
    @tailrec
    def loop(n: Int): Boolean =
      if (n >= as.length - 1) true
      else if (f(as(n), as(n + 1))) false
      else loop(n + 1)

    loop(0)
  }
}
