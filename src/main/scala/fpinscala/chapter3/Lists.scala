package fpinscala.chapter3

import scala.annotation.tailrec

object Lists {
  def tail[A](ls: Seq[A]): Seq[A] = ls match {
    case Nil => Nil
    case _ :: t => t
  }

  def setHead[A](xs: Seq[A], n: A): Seq[A] = xs match {
    case Nil => Nil
    case _ :: t => n +: t
  }

  def drop[A](xs: Seq[A], n: Int): Seq[A] =
    if (n <= 0) xs
    else xs match {
      case Nil => Nil
      case _ :: t => drop(t, n-1)
    }

  @tailrec
  def dropWhile[A](xs: Seq[A], f: A => Boolean): Seq[A] = xs match {
    case h :: t if f(h) => dropWhile(t, f)
    case _ => xs
  }

  def init[A](l: Seq[A]): Seq[A] = l match {
    case Nil => Nil
    case h :: Nil => Nil
    case h :: t => h +: init(t)
  }

  def foldRight[A,B](as: Seq[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case h :: t => f(h, foldRight(t, z)(f))
  }

  def length[A](as: Seq[A]): Int = foldRight(as, 0)((_, acc) => acc + 1)

  @tailrec
  def foldLeft[A,B](as: Seq[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case h :: t => foldLeft(t, f(h, z))(f)
  }

  def sumLeft(as: Seq[Int]): Int = foldLeft(as, 0)((a,acc) => a + acc)

  def productLeft(as: Seq[Int]): Int = foldLeft(as, 1)((a, acc) => a * acc)

  def lengthLeft[A](as: Seq[A]): Int = foldLeft(as, 0)((_, acc) => acc + 1)

  def reverseLeft[A](as: Seq[A]): Seq[A] = foldLeft(as, Seq.empty[A])((e,xs) => e +: xs)

  def foldRightLeft[A,B](as: Seq[A], z: B)(f: (A, B) => B): B =
    foldLeft(reverseLeft(as), z)((a,b) => f(a, b))

  def appendLeft[A](as: List[A], xs: List[A]): List[A] = foldRightLeft(as, xs)((e, acc) => e :: acc)

  def concat[A](ls: List[List[A]]): List[A] = foldRightLeft(ls, List.empty[A])(appendLeft)

  def add1(as: Seq[Int]): Seq[Int] = foldLeft(as, Seq.empty[Int])((e, acc) => acc :+ (e + 1))

  def double2string(as: Seq[Double]): Seq[String] = foldLeft(as, Seq.empty[String])((e, acc) => acc :+ e.toString)

  def mapAsFold[A,B](as: Seq[A])(f: A => B): Seq[B] =
    foldLeft(as, Seq.empty[B])((e, acc) => acc :+ f(e))

  def filterAsFold[A](as: Seq[A])(f: A => Boolean): Seq[A] =
    foldLeft(as, Seq.empty[A])((e, acc) => if (f(e)) acc :+ e else acc)

  def flatMap[A,B](as: Seq[A])(f: A => Seq[B]): Seq[B] =
    foldLeft(as, Nil:Seq[Seq[B]])((e, acc) => acc :+ f(e)).flatten

  def filterAsFlatMap[A](as: Seq[A])(f: A => Boolean): Seq[A] =
    flatMap(as)(a => if (f(a)) List(a) else Nil)
}
